require 'test_helper'

class SearchControllerTest < ActionDispatch::IntegrationTest
  test "should get query:string" do
    get search_query:string_url
    assert_response :success
  end

end
